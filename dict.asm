%include "lib.inc"
%define qw 8
global find_word

find_word:
	push r12
.loop:	
	mov r12, rsi
	add rsi, QWORD
	call string_equals
	test rax, rax
	jz .next_loop
	mov rax, r12
    je .end
.next_loop:
	mov	 rsi, r12
	mov rsi, [rsi]
	test rsi, rsi
	jnz .loop
	xor rax, rax
	je .end
.end:
    pop r12
	ret
