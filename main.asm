%include "dict.inc"
%include "words.inc"
%include "lib.inc"

%define BUFFER_SIZE_MAX 256
%strlen ERR1_SIZE 17
%strlen ERR2_SIZE 12
%define qw 8
%define SYSCALL_WRITE 1
%define STDERR 2

section .bss

buffer: resb BUFFER_SIZE_MAX

section .rodata

err1: db "line can not read", 10, 0
err2: db "el not found", 10, 0

section .text

global _start
 
_start:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE_MAX
    call read_word
    test rax, rax
    je .err1_loop
 
    mov rdi, rax
    mov rsi, first_word
    call find_word

    test rax, rax
    je .err2_loop
    je .next
.next:   
    mov r12, rax        
    mov rdi, [rax + qw]
    call string_length

    mov rdi, 0
    mov rdi, [r12 + qw]
    lea rdi, [rdi + rax]
    inc rdi
    call print_string
    call print_newline
    call exit
.err1_loop:
    mov rax, SYSCALL_WRITE
    mov rdi, STDERR
    mov rsi, err1
    mov rdx, ERR1_SIZE
    syscall
    call exit
.err2_loop:
    mov rax, SYSCALL_WRITE
    mov rdi, STDERR
    mov rsi, err2
    mov rdx, ERR2_SIZE
    syscall
