%define next_el 0

%macro colon 2
    %2:
        dq next_el
        db %1, 0
    %define next_el %2
%endmacro
