import subprocess
FAIL = '\033[91m'
OK = '\033[92m'
ENDC = '\033[0m'
tests = ['aaaa', 'fd;lgjkdfg', '12345234', 'a'*255,'a'*256,'first', 'second','third', 'aaa bbb', 'first word']

ElNotFoundErr = "el not found"
OverflowErr = "line can not read"

expected = [
    ElNotFoundErr,
    ElNotFoundErr,
    ElNotFoundErr,
    ElNotFoundErr,
    OverflowErr,
    'first data' + '\n',
    'second data' + '\n',
    'third data' + '\n'
]



print(" ---  Tests started!  --- ")
print('\n')
fileStartCommand = './app'
failures = 0
UTF = 'utf_8'
for i in range(0, len(tests)):
    print("Test "  + str((i + 1)) + ": ", end='')
    result = subprocess.run([fileStartCommand], input = tests[i].encode(UTF) + b'\n', capture_output = True)

    if (i < 5):
        output = result.stderr
    else:
        output = result.stdout
    if (output == expected[i].encode(UTF)):
        print('\033[92m' + 'OK' + '\033[0m' + '\n')
    else:
        failures += 1
        print('\033[91m' + 'FAIL' + '\033[0m' + '\n')

if (failures >= 1):
    print('\033[91m' + f'\n --- {failures} failed --- \n' + '\033[0m'  + '\n')
else:
    print('\033[92m' + ' --- cases are passed! --- ' + '\033[0m' + '\n')
